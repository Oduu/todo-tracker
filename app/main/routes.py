from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
from app import mongo

main = Blueprint('main', __name__)