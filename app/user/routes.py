from app import mongo, jwt
from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, abort, session, g, jsonify, make_response
from flask_bcrypt import bcrypt
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from flask_cors import CORS, cross_origin
from functools import wraps

user = Blueprint('user', __name__)

@user.route("/users/register", methods=['POST'])
@cross_origin()
def signup():
	users = mongo.db.users

	username = request.get_json()['username']
	password = request.get_json()['password']
	hashed = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
	email = request.get_json()['email']

	exists = users.find_one({'username': username})

	if exists == None:
		users.insert({'username' : username, 'password': hashed, 'email': email})

		result = {
			'username': username,
			'email': email,
			'password': password
		}

	else:
		return "Username already exists"

	return "User Created"

@user.route("/users/login", methods=['POST'])
@cross_origin()
def login():
	users = mongo.db.users

	username = request.get_json()['username']
	text_password = request.get_json()['password']

	exists = users.find_one({'username': username})

	if exists:
		if bcrypt.checkpw(text_password.encode('utf-8'), exists['password'].encode('utf-8')):
			access_token = create_access_token(identity = {'username': exists['username'], 'email': exists['email']}, expires_delta=False)
			result = jsonify(access_token)
	else:
		result = jsonify({"error": "Invalid username or password"})

	return result

@user.route("/users/auth", methods=['GET'])
@cross_origin(headers=['Authorization'])
@jwt_required
def auth():
	identity = get_jwt_identity()
	if identity == None:
		return "hello", 400
	else:
		current_user = identity['username']
		return jsonify({"current_user": current_user})
		
	# expired:
	# eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Nzg5MTQ4NTgsIm5iZiI6MTU3ODkxNDg1OCwianRpIjoiNjIwNzI3MmUtY2U2My00ZDhlLTk5N2UtZjYyMTFiYTdiZjlkIiwiZXhwIjoxNTc4OTE1NzU4LCJpZGVudGl0eSI6eyJ1c2VybmFtZSI6Im9kdSIsImVtYWlsIjoidGVzdEB0ZXN0In0sImZyZXNoIjpmYWxzZSwidHlwZSI6ImFjY2VzcyJ9.cOyMN3-DQsMGnVtK6AqJ_mz_X9LvTwtWarz7aJuBcB4