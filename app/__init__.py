from flask import Flask
from flask_pymongo import PyMongo
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_cors import CORS, cross_origin
import os

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'todo'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/todo'
app.config['JWT_SECRET_KEY'] = b'\xebkY\x8e\xb7\xf5\xa3\xbe\xfa(|M\x186\x12v\xba\x04aGuEX\xd0'

mongo = PyMongo(app)
bcrpyt = Bcrypt(app)
jwt = JWTManager(app)
CORS(app)

app.secret_key = b'\xebkY\x8e\xb7\xf5\xa3\xbe\xfa(|M\x186\x12v\xba\x04aGuEX\xd0'

from app.main.routes import main
from app.user.routes import user
from app.task.routes import task

app.register_blueprint(main)
app.register_blueprint(user)
app.register_blueprint(task)