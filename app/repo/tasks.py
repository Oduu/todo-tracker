from .mongo import MongoRepo
from datetime import datetime

import uuid

class TaskRepo(MongoRepo):
	def __init__(self):
		super().__init__('tasks')

	def get_by_username(self, username):
		result = self.col.find({"username": username}).sort("position")
		return result

	def get_by_task(self, username, task_id):
		result = self.col.find({'_id': task_id, 'username': username})
		return result

	def _count_tasks_username(self, username):
		result = self.col.count_documents({'username': username})
		return result

	def _count_tasks_task(self, task_id):
		result = self.col.count_documents({'_id': task_id})
		return result

	def insert_task(self, username, title):
		new_task = {'username': username, 'title': title, '_id': str(uuid.uuid4()),
					'created': datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M'), 'position': self._count_tasks_username(username), 'completed': False}
		self.col.insert(new_task)
		return new_task

	def delete_by_id(self, username, task_id):
		self.col.remove({'_id': task_id, 'username': username})
		result = "", 204
		return result

	def update_task(self, task_id, username, new_title):
		self.col.find_one_and_update({'_id': task_id, 'username': username}, {"$set": {"title": new_title}})
		result = self.col.find({'_id': task_id, 'username': username})
		return result

	def move_task_up(self, username, old_pos, new_pos):
		result = self.col.update_many(
			{'username': username, 'position': {"$gte": new_pos, "$lt": old_pos}},
			{"$inc": {'position': 1}})
		# add one to all of these
		return result

	def move_task_down(self, username, old_pos, new_pos):
		result = self.col.update_many(
			{'username': username, 'position': {"$gt": old_pos, "$lte": new_pos}},
			{"$inc": {'position': -1}})
		# minus one from all these
		return result

	def update_pos(self, username, task_id, new_pos):
		self.col.find_one_and_update({'_id': task_id, 'username': username}, {"$set": {"position": new_pos}})
		result = self.col.find({'_id': task_id, 'username': username})
		return result

	def complete_task(self, username, task_id, status):
		self.col.find_one_and_update({'_id': task_id, 'username': username}, {"$set": {'completed': status}})
		result = self.col.find({'_id': task_id, 'username': username})
		return result