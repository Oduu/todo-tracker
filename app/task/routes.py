from app import mongo, jwt
from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, abort, session, g, jsonify, make_response
from flask_bcrypt import bcrypt
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from flask_cors import CORS, cross_origin
from functools import wraps
from datetime import datetime
import uuid
from app.repo.tasks import TaskRepo

task = Blueprint('task', __name__)

@task.route("/tasks", methods=['GET'])
@cross_origin(headers=['Authorization'])
@jwt_required
def get_tasks():
	current_user = get_jwt_identity()['username']

	tasks = TaskRepo().get_by_username(current_user)

	items = []

	for item in tasks:
		items.append(item)

	return jsonify(items)

@task.route("/tasks", methods=['POST'])
@cross_origin(headers=['Authorization'])
@jwt_required
def new_task():
	current_user = get_jwt_identity()['username']

	data = request.get_json()

	if 'title' in data:
		title = data['title']
		new_task = TaskRepo().insert_task(current_user, title)
		result = jsonify(new_task)
	else:
		result = jsonify({"error": "You did not provide a task title"}), 400

	return result


@task.route("/tasks/<task_id>", methods=['GET'])
@cross_origin(headers=['Authorization'])
@jwt_required
def get_by_id(task_id=None):
	current_user = get_jwt_identity()['username']
	
	exists = TaskRepo().get_by_task(current_user, task_id)
	items = []

	for item in exists:
		items.append(item)
		result = jsonify(items)

	if items == []:
		result = jsonify({"error": "No task was found with the given id"}), 404

	return result

@task.route("/tasks/<task_id>", methods=['DELETE'])
@cross_origin(headers=['Authorization'])
@jwt_required
def delete_by_id(task_id=None):
	current_user = get_jwt_identity()['username']

	result = TaskRepo().delete_by_id(current_user, task_id)

	return result

@task.route("/tasks/<task_id>", methods=['PATCH'])
@cross_origin(headers=['Authorization'])
@jwt_required
def patch_by_id(task_id=None):
	items = []
	current_user = get_jwt_identity()['username']

	data = request.get_json()

	if 'title' in data:
		new_title = data['title']
		updated = TaskRepo().update_task(task_id, current_user, new_title)
		for item in updated:
			items.append(item)
			result=jsonify(items)
	else:
		result = jsonify({"error": "You did not provide a task title"}), 400
		
	return result

@task.route("/tasks/move", methods=['PATCH'])
@cross_origin(headers=['Authorization'])
@jwt_required
def task_move():
	current_user = get_jwt_identity()['username']
	data = request.get_json()

	if 'old' and 'new' and 'id' in data:
		old_pos = data['old']
		new_pos = data['new']
		task_id = data['id']
		if old_pos > new_pos: # moved up
			query1 = TaskRepo().move_task_up(current_user, old_pos, new_pos)
			query2 = TaskRepo().update_pos(current_user, task_id, new_pos)

		else: # moved down
			query1 = TaskRepo().move_task_down(current_user, old_pos, new_pos)
			query2 = TaskRepo().update_pos(current_user, task_id, new_pos)

		return jsonify({"message": "You're task has been moved"})
	else:
		return jsonify({"error": "You did not provide the required fields"}), 400

@task.route("/tasks/complete", methods=['PATCH'])
@cross_origin(headers=['Authorization'])
@jwt_required
def complete_task():
	current_user = get_jwt_identity()['username']
	data = request.get_json()

	if 'id' and 'status' in data:
		task_id = data['id']
		status = data['status']
		update_task = TaskRepo().complete_task(current_user, task_id, status)
		return jsonify({"success": "status updated"}), 200
	else:
		return jsonify({"error": "You did not provide the required fields"}), 400
	

