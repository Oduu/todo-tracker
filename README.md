Basic todo list using Vue.js for frontend and python/flask with MongoDB for backend.

##Technology used:

    - Vue
    - MongoDB
    - Python
    - JSON Web Tokens
    - JQuery
    - Axios
    - Flask
    - Bootstrap
    - CSS

#Features:


##Main page

![](https://i.imgur.com/ZkVdMrA.png)

This is where the user will add, remove, complete and edit tasks. 

To add a task, the user just needs to type the subject of the of the task in the
"New task" box and press enter.

To complete a task the user needs to press the tick icon on the task which will
add the line-through text decorator to the task and update the task in the DB
through an axios request.

(Icons used are from the Octicons library installed via NPM)

##Edit Task

![](https://i.imgur.com/ZkVdMrA.png)

When the edit task button is clicked a form is shown instead of just the
header containing the text of the task.

