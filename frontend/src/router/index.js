import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Profile from '../views/Profile.vue'
import About from '../views/About.vue'
import Tasks from '../views/Tasks.vue'

Vue.use(VueRouter)

const routes = [

  {	path: '/',	name: 'login',	component: Login },
  {	path: '/about',	name: 'about', component: About},
  {	path: '/home',	name: 'home',	component: Home },
  {	path: '/profile',	name: 'profile',	component: Profile },
  {	path: '/tasks',	name: 'tasks',	component: Tasks },
  {	path: '/register',	name: 'register',	component: Register },
  { path: '*', name: 'NotFound', component: Tasks } // This needs to always be at the bottom of the routes list

]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
