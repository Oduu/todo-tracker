import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'

Vue.component('Navbar', require('./components/Navbar.vue').default)

Vue.config.productionTip = false

// import global style sheet
import "@/assets/styles/global.css"

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
